# Qdrant People Search (Week 7)

Qdrant People Search is a Rust project that demonstrates how to utilize Qdrant, an approximate nearest neighbor search engine, to search for people based on their age. This project is designed to showcase the integration of Qdrant within a Rust application, focusing on creating a collection of people and querying individuals based on their age.

## Introduction

Qdrant is a powerful tool for performing similarity search on large datasets. In this project, we utilize Qdrant to create a collection of people, each represented by a vector containing their age. By ingesting this data into Qdrant, we can efficiently query the collection to retrieve individuals matching specific criteria, such as age.

## Features

- **Creating a Collection**: The project demonstrates how to create a collection named "people" in Qdrant. This collection is configured to store vectors representing people, with each vector containing their age.
  
- **Ingesting Data**: Hardcoded entries of people, including their ID, name, and age, are ingested into the Qdrant collection. Each person's age is represented as a component of their vector.
  
- **Searching by Age**: The project performs a search in the Qdrant collection to find people who are of a specific age. In this case, it searches for individuals who are 25 years old.

## Installation

1. Clone the repository:

   ```
   git clone https://gitlab.com/dukeaiml/IDS721/fj49_week7.git
   ```


2. Build and run the project using Cargo:

   ```
   cargo run
   ```

   Ensure that you have a Qdrant server running locally at `http://localhost:6334` or update the URL accordingly in the code.

## Usage

1. Modify the `main.rs` file to customize the hardcoded entries of people, including their ID, name, and age.
  
2. Adjust the search parameters in the `main.rs` file to perform searches based on different age criteria or other attributes.

3. Run the project using Cargo:

   ```
   cargo run
   ```

4. View the output to see the names of people who match the specified age criteria.

## Dependencies

This project relies on the following dependencies:

- [qdrant-client](https://crates.io/crates/qdrant-client): A Rust client library for Qdrant.
- [serde_json](https://crates.io/crates/serde_json): A JSON serialization library for Rust.

These dependencies are managed via Cargo and will be installed automatically when building the project.

## Screenshots

1. Collection:

![Screenshot 2](screenshots/collection.png)

2. Content:

![Screenshot](screenshots/content.png)

3. Query result

![Screenshot](screenshots/query_result.png)