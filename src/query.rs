// query.rs

use reqwest::Client;

pub async fn retrieve_youngest_people(client: &Client, limit: usize) -> Result<(), reqwest::Error> {
    let query = r#"{"filters": [{"range": {"attribute": "age", "lt": 25}}], "limit": 2}"#;
    
    let response = client.post("http://localhost:6333/v1/search")
        .body(query)
        .send()
        .await?;
    
    println!("Youngest people:");
    println!("{}", response.text().await?);
    
    Ok(())
}
